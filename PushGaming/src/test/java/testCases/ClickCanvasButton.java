package testCases;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import hooks.Hook;
import utilities.CommonUtil;

public class ClickCanvasButton extends Hook{
  @Test
  public void clickButton(ITestContext context) {
	  
	  	
	  	// Driver from Test Context
	  	WebDriver driver = (WebDriver) context.getAttribute("WebDriver");
	  	
	  	
	  	Actions actions = new Actions(driver);
	  	JavascriptExecutor js = (JavascriptExecutor) driver;  

	  	/*
		 * Logic to find center of X and Y coordinates as per the size of Canvas
		 * X Coordinate Center  = Width /2
		 * Y Coordinate Center  = Height /2  
		 */	  	
		int x_center_cordinate = Math.round(driver.findElement(By.xpath("/html/body/canvas")).getSize().getWidth() / 2);
		int y_center_cordinate = Math.round(driver.findElement(By.xpath("/html/body/canvas")).getSize().getHeight() / 2);

		/*
		 * Set contain unique set of X and Y coordinates respectively
		 * Coordinates are generated with the help of Canvas Size and deviation mentioned in config.properties file 
		 * Config Files Path : src/test/resources/Config/config.properties
		 */
		Set <Integer> xCoordinate = CommonUtil.getCanvasCoordinates(x_center_cordinate); 
		Set <Integer> yCoordinate = CommonUtil.getCanvasCoordinates(y_center_cordinate); 

		WebElement canvas = driver.findElement(By.xpath("/html/body/canvas"));
		
		/*
		 * Loop to perform click on various combination of (X,Y) coordinates as per Set  xCoordinate, yCoordinate
		 */
		
		for ( int x : xCoordinate){		
			for ( int y : yCoordinate){
				actions.moveToElement(canvas, x, y).click().perform();
				
				//String str =js.executeScript("return document.getElementsByTagName('canvas')[0].style.backgroundColor;").toString();
							
				System.out.println("Clicked on Coordinate X:" + x + " ,Y:" + y );
							
				
			}
		}

  
  }
}
