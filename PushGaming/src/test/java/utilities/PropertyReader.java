package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class PropertyReader {
	
		 
	   private final Properties configProp = new Properties();
	   
	   
	   private PropertyReader()
	   {
		   try {
		   InputStream in = new FileInputStream("./src/test/resources/Config/config.properties");
	      
	          configProp.load(in);
	      } catch (Exception e) {
	          e.printStackTrace();
	      }
	   }
	 
	   private static class Holder
	   {
		   private static final PropertyReader INSTANCE = new PropertyReader();
	   }
	 
	   public static PropertyReader getInstance()
	   {
	      return Holder.INSTANCE;
	   }
	    
	   public String getProperty(String key){
	      return configProp.getProperty(key);
	   }
	    
	   public Set<String> getAllPropertyNames(){
	      return configProp.stringPropertyNames();
	   }
	    
	   public boolean containsKey(String key){
	      return configProp.containsKey(key);
	   }


}
