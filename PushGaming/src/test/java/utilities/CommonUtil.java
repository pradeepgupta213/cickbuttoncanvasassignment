package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.WebElement;

public class CommonUtil {
	
	public static Set getCanvasCoordinates(int center_coordinate) {
		
		//Fetch Deviation value from config.properties files
		int deviation = Integer.parseInt(PropertyReader.getInstance().getProperty("CoordinateDeviation"));
		
		Set <Integer> coordinateSet = new HashSet<Integer>();
		
		int init_count = 0;

		//While loop to build a set of coordinates as per the deviation mentioned in config file 
		while (center_coordinate > 0) {

			if (init_count == 0) {

				coordinateSet.add(center_coordinate);
				init_count++;
			}
			coordinateSet.add(center_coordinate - deviation);
			coordinateSet.add((center_coordinate - deviation) * -1);
			center_coordinate -= deviation;
		}

		return coordinateSet;
		
	}
	
	
	

}
