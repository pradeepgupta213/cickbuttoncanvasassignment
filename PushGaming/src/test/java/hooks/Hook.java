package hooks;

import org.testng.annotations.Test;

import utilities.CommonUtil;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;

public class Hook {

	private WebDriver driver;

	@BeforeTest
	@Parameters({ "browserName", "browserVersion", "url" })
    /*
     * Method to Initialised driver as per Browser Name and Version 
     */
	
	public void beforeTest(ITestContext context, String browserName, String browserVersion, String url)   {

		
		if (browserName.equalsIgnoreCase("Chrome")) {

			System.setProperty("webdriver.chrome.driver",
					"./src/test/resources/Drivers/ChromeDrivers/Chrome" + browserVersion + "/chromedriver.exe");
			driver = new ChromeDriver();

			driver.get(url);
			driver.manage().window().maximize();
			
			context.setAttribute("WebDriver",driver);
		}

	}

	@AfterTest
	public void afterTest() {

		driver.quit();
	}

}
